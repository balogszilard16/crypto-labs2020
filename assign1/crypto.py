"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Balog Szilard
SUNet: <SUNet ID>

Replace this placeholder text with a description of this module.
"""
import utils


#################
# CAESAR CIPHER #
#################

def encrypt_caesar(plaintext):
    """Encrypt a plaintext using a Caesar cipher.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.

    utils.find_input_errors(plaintext)

    encrypted = ""

    for i in plaintext:
        if 'A' <= i <= 'Z':
            encrypted += chr((ord(i) - ord('A') + 3) % 26 + ord('A'))
        elif 'a' <= i <= 'z':
            encrypted += chr((ord(i) - ord('a') + 3) % 26 + ord('a'))
        else:
            encrypted += i

    return encrypted


def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.

    utils.find_input_errors(ciphertext)

    decrypted = ""

    for i in ciphertext:
        if 'A' <= i <= 'Z':
            decrypted += chr((ord(i) - ord('A') + 23) % 26 + ord('A'))
        elif 'a' <= i <= 'z':
            decrypted += chr((ord(i) - ord('a') + 23) % 26 + ord('a'))
        else:
            decrypted += i

    return decrypted


plaintext = "F1RST P0ST"
print(f"Plain text: {plaintext}")
encryptedCaesar = encrypt_caesar(plaintext)
print(f"Encrypted Caesar: {encryptedCaesar}")
decryptedCaesar = decrypt_caesar(encryptedCaesar)
print(f"Decrypted Caesar: {decryptedCaesar}\n")


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param plaintext: The message to encrypt.
    :type plaintext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The encrypted ciphertext.
    """
    # Your implementation here.

    utils.find_input_errors(plaintext)

    encrypted = ""
    keyword_index = 0
    keyword_length = len(keyword)
    for i in plaintext:
        keyword_letter = keyword[keyword_index]
        if 'A' <= i <= 'Z':
            encrypted += chr((ord(i) + ord(keyword_letter) - 2 * ord('A')) % 26 + ord('A'))
        elif 'a' <= i <= 'z':
            encrypted += chr((ord(i) + ord(keyword_letter) - 2 * ord('a')) % 26 + ord('a'))
        else:
            encrypted += i
        keyword_index = (keyword_index + 1) % keyword_length

    return encrypted


def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.

    :param ciphertext: The message to decrypt.
    :type ciphertext: str
    :param keyword: The key of the Vigenere cipher.
    :type keyword: str

    :returns: The decrypted plaintext.
    """
    # Your implementation here.

    utils.find_input_errors(ciphertext)

    decrypted = ""
    keyword_index = 0
    keyword_length = len(keyword)
    for i in ciphertext:
        keyword_letter = keyword[keyword_index]
        if 'A' <= i <= 'Z':
            decrypted += chr((ord(i) + 26 - ord(keyword_letter)) % 26 + ord('A'))
        elif 'a' <= i <= 'z':
            decrypted += chr((ord(i) + 26 - ord(keyword_letter)) % 26 + ord('a'))
        else:
            decrypted += i
        keyword_index = (keyword_index + 1) % keyword_length

    return decrypted


########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

plaintext = "ATTACKATDAWN"
keyword = "LEMON"
print(f"Plain text: {plaintext}")
encryptedVignere = encrypt_vigenere(plaintext, keyword)
print(f"Encrypted Vignere: {encryptedVignere}")
decryptedVignere = decrypt_vigenere(encryptedVignere, keyword)
print(f"Decrypted Vignere: {decryptedVignere}\n")


def generate_private_key(n=8):
    """Generate a private key to use with the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key
    components of the MH Cryptosystem. This consists of 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        Note: You can double-check that a sequence is superincreasing by using:
            `utils.is_superincreasing(seq)`
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q`
        Note: You can use `utils.coprime(r, q)` for this.

    You'll also need to use the random module's `randint` function, which you
    will have to import.

    Somehow, you'll have to return all three of these values from this function!
    Can we do that in Python?!

    :param n: Bitsize of message to send (defaults to 8)
    :type n: int

    :returns: 3-tuple private key `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    # Your implementation here.
    raise NotImplementedError('generate_private_key is not yet implemented!')


def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in
    the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one or two lines using list comprehensions.

    :param private_key: The private key created by generate_private_key.
    :type private_key: 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    :returns: n-tuple public key
    """
    # Your implementation here.
    raise NotImplementedError('create_public_key is not yet implemented!')


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    Following the outline of the handout, you will need to:
    1. Separate the message into chunks based on the size of the public key.
        In our case, that's the fixed value n = 8, corresponding to a single
        byte. In principle, we should work for any value of n, but we'll
        assert that it's fine to operate byte-by-byte.
    2. For each byte, determine its 8 bits (the `a_i`s). You can use
        `utils.byte_to_bits(byte)`.
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk of the message.

    Hint: Think about using `zip` and other tools we've discussed in class.

    :param message: The message to be encrypted.
    :type message: bytes
    :param public_key: The public key of the message's recipient.
    :type public_key: n-tuple of ints

    :returns: Encrypted message bytes represented as a list of ints.
    """
    # Your implementation here.
    raise NotImplementedError('encrypt_mh is not yet implemented!')


def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key.

    Following the outline of the handout, you will need to:
    1. Extract w, q, and r from the private key.
    2. Compute s, the modular inverse of r mod q, using the Extended Euclidean
        algorithm (implemented for you at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum problem using c' and w to recover
        the original plaintext byte.
    5. Reconstitute the decrypted bytes to form the original message.

    :param message: Encrypted message chunks.
    :type message: list of ints
    :param private_key: The private key of the recipient (you).
    :type private_key: 3-tuple of w, q, and r

    :returns: bytearray or str of decrypted characters
    """
    # Your implementation here.
    raise NotImplementedError('decrypt_mh is not yet implemented!')


def encrypt_scytale(plaintext, circumference):
    utils.find_input_errors(plaintext)

    encrypted = ""

    len_plaintext = len(plaintext)
    if len_plaintext % circumference != 0:
        for i in range(circumference - len_plaintext % circumference):
            plaintext += "+"
    len_plaintext = len(plaintext)

    row = 0
    column = 0
    letter_index = 0
    for i in plaintext:
        current_letter = plaintext[letter_index]
        if current_letter != '+':
            encrypted += current_letter
        column += circumference
        row += column // len_plaintext
        column %= len_plaintext
        letter_index = column + row

    return encrypted


def decrypt_scytale(ciphertext, circumference):
    utils.find_input_errors(ciphertext)

    decrypted = ""

    len_ciphertext = len(ciphertext)
    rows = len_ciphertext // circumference
    if rows * circumference != len_ciphertext:
        rows += 1
    cols = circumference

    scytale = [[0 for j in range(cols)] for i in range(rows)]

    remaining_letters = len_ciphertext % circumference
    index = 0
    for letter in ciphertext:
        i = index % rows
        j = index // rows
        index += 1
        if 0 < remaining_letters <= j and i == rows - 1:
            i = 0
            j += 1
            index += 1
        scytale[i][j] = letter

    index = 0
    for letter in ciphertext:
        i = index // cols
        j = index % cols
        decrypted += scytale[i][j]
        index += 1

    return decrypted


plaintext = "IAMHURTVERYBADLYHELP"
circumference = 5
print(f"Plain text: {plaintext}")
encryptedScytale = encrypt_scytale(plaintext, circumference)
print(f"Encrypted Scytale: {encryptedScytale}")
decryptedScytale = decrypt_scytale(encryptedScytale, circumference)
print(f"Decrypted Scytale: {decryptedScytale}\n")


def encrypt_railfence(plaintext, rails):
    utils.find_input_errors(plaintext)

    encrypted = ""

    rows = rails
    cols = len(plaintext)

    zigzag = [[0 for j in range(cols)] for i in range(rows)]

    increment = 1
    i = 0
    j = 0
    for letter in plaintext:
        zigzag[i][j] = letter
        i += increment
        j += 1
        if i == rows - 1 or i == 0:
            increment *= -1

    for i in range(rows):
        for j in range(cols):
            if zigzag[i][j] != 0:
                encrypted += zigzag[i][j]

    return encrypted


def decrypt_railfence(ciphertext, rails):
    utils.find_input_errors(ciphertext)

    decrypted = ""
    rows = rails
    cols = len(ciphertext)

    zigzag = [[0 for j in range(0, cols)] for i in range(0, rows)]

    increment = 1
    i = 0
    j = 0
    for letter in ciphertext:
        zigzag[i][j] = -1
        i += increment
        j += 1
        if i == rows - 1 or i == 0:
            increment *= -1

    index = 0
    for i in range(rows):
        for j in range(cols):
            if zigzag[i][j] == -1:
                zigzag[i][j] = ciphertext[index]
                index += 1

    increment = 1
    i = 0
    j = 0
    for letter in ciphertext:
        decrypted += zigzag[i][j]
        i += increment
        j += 1
        if i == rows - 1 or i == 0:
            increment *= -1

    return decrypted


plaintext = "WEAREDISCOVEREDFLEEATONCE"
rails = 3
print(f"Plain text: {plaintext}")
encryptedRailfence = encrypt_railfence(plaintext, rails)
print(f"Encrypted Railfence: {encryptedRailfence}")
decryptedRailfence = decrypt_railfence(encryptedRailfence, rails)
print(f"Decrypted Railfence: {decryptedRailfence}\n")


def encrypt_caesar_non_textfile(input_file, output_file):
    try:
        f = open(input_file, 'rb')

        data = []
        header = f.read(8)
        byte = f.read(1)
        while byte:
            data.append(byte)
            byte = f.read(1)
        f.close()

        encrypted = []
        for byte in data:
            i = int.from_bytes(byte, byteorder='big', signed=False)
            i = (i + 3) % 256
            # i = i.to_bytes(1, byteorder='big')
            encrypted.append(i)

        f = open(output_file, 'wb')
        f.write(bytearray(header))
        f.write(bytearray(encrypted))

        # for i in encrypted:
        #     f.write(i)

        f.close()

    except FileNotFoundError:
        print('File not found')
        exit(1)


def decrypt_caesar_non_textfile(input_file, output_file):
    try:
        f = open(input_file, 'rb')

        data = []
        header = f.read(8)
        byte = f.read(1)
        while byte:
            data.append(byte)
            byte = f.read(1)
        f.close()

        encrypted = []
        for byte in data:
            i = int.from_bytes(byte, byteorder='big', signed=False)
            i = (i + 253) % 256
            # i = i.to_bytes(1, byteorder='big')
            encrypted.append(i)

        f = open(output_file, 'wb')
        f.write(header)
        f.write(bytearray(encrypted))

        # for i in encrypted:
        #     f.write(i)

        f.close()

    except FileNotFoundError:
        print('File not found')
        exit(1)


encrypt_caesar_non_textfile("photo.png", 'encrypted.png')
decrypt_caesar_non_textfile("encrypted.png", 'decrypted.png')
